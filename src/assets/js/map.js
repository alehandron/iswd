
let map;
function initMap() {
	map = new google.maps.Map( document.getElementById( 'map' ), {
		center: {

			lat: 47.94007800944945,
			lng: 33.41735553318371
		},
		zoom: 14.3,
        mapId: 'a2e920be4e63fca0',
        mapTypeControl: false,
        disableDefaultUI: true
	});

    let image = "assets/images/marker.svg";
    new google.maps.Marker({
        position: {lat:47.939939, lng:33.407490},
        map,
        icon: image,

      });
}
initMap();
